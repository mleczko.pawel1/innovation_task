<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Entity\Currency;
use App\Entity\ValueObject\ExchangeRate;
use App\Repository\CurrencyRepositoryInterface;
use App\Service\CurrencyParserInterface;
use App\Service\ExchangeRatesClientInterface;
use App\Service\ExchangeRatesService;
use Money\Currency as MoneyCurrency;
use Money\Money;
use PHPUnit\Framework\TestCase;

class ExchangeRatesServiceTest extends TestCase
{
    public function testProcessRates()
    {
        // Mock the dependencies
        $clientMock = $this->getMockBuilder(ExchangeRatesClientInterface::class)
            ->getMock();

        $currencyRepositoryMock = $this->getMockBuilder(CurrencyRepositoryInterface::class)
            ->getMock();

        $currencyParserMock = $this->getMockBuilder(CurrencyParserInterface::class)
            ->getMock();

        // Set up the expected exchange rates data
        $exchangeRatesRawData = [
            [
                'rates' => [
                    [
                        'code' => 'USD',
                        'currency' => 'US Dollar',
                        'rate' => '1.23'
                    ],
                    [
                        'code' => 'EUR',
                        'currency' => 'Euro',
                        'rate' => '0.98'
                    ]
                ]
            ]
        ];

        $usdCurrency = new Currency('US Dollar');
        $eurCurrency = new Currency('Euro');

        // Set up the expected currency objects and exchange rates
        $usdMoneyCurrency = new MoneyCurrency('USD');
        $usdExchangeRate = new ExchangeRate(new Money('123', $usdMoneyCurrency));

        $eurMoneyCurrency = new MoneyCurrency('EUR');
        $eurExchangeRate = new ExchangeRate(new Money('98', $eurMoneyCurrency));

        $usdCurrency->setExchangeRate($usdExchangeRate);
        $eurCurrency->setExchangeRate($eurExchangeRate);

        // Set up the expected method calls on the mock objects
        $clientMock->expects($this->once())
            ->method('fetch')
            ->willReturn($exchangeRatesRawData);

        $currencyRepositoryMock->expects($this->exactly(2))
            ->method('getByCurrencyCode')
            ->willReturnOnConsecutiveCalls($usdCurrency, $eurCurrency);

        $currencyRepositoryMock->expects($this->exactly(2))
            ->method('save');

        $currencyParserMock->expects($this->exactly(2))
            ->method('parse')
            ->willReturnOnConsecutiveCalls(new Money('123', $usdMoneyCurrency), new Money('98', $eurMoneyCurrency));

        // Create an instance of the ExchangeRatesService and call the method we want to test
        $exchangeRatesService = new ExchangeRatesService($clientMock, $currencyRepositoryMock, $currencyParserMock);
        $exchangeRatesService->processRates();

        // Assert that the currency objects have the expected exchange rates set
        $this->assertEquals($usdExchangeRate, $usdCurrency->getExchangeRate());
        $this->assertEquals($eurExchangeRate, $eurCurrency->getExchangeRate());
    }
}

