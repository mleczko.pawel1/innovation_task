<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Service\CurrencyParser;
use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Money;
use PHPUnit\Framework\TestCase;

class CurrencyParserTest extends TestCase
{
    public function testParse(): void
    {
        $rawCurrencyData = [
            'code' => 'USD',
            'mid' => 3.7856,
        ];

        $currencies = new ISOCurrencies();
        $currencyParser = new CurrencyParser();

        $expectedMoney = new Money(379, new Currency('USD'));
        $parsedMoney = $currencyParser->parse($rawCurrencyData);

        $this->assertEquals($expectedMoney, $parsedMoney);
    }
}

