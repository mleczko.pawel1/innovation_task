<?php

declare(strict_types=1);

namespace App\Tests\Service;

use Exception;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use App\Service\NBPApiClient;

class NBPApiClientTest extends TestCase
{
    public function testFetch()
    {
        // Mock the HttpClientInterface and ResponseInterface dependencies
        $httpClientMock = $this->getMockBuilder(HttpClientInterface::class)
            ->getMock();

        $responseMock = $this->getMockBuilder(ResponseInterface::class)
            ->getMock();

        // Set up the expected response data
        $responseData = [
            [
                'rates' => [
                    [
                        'code' => 'USD',
                        'currency' => 'US Dollar',
                        'rate' => '1.23'
                    ],
                    [
                        'code' => 'EUR',
                        'currency' => 'Euro',
                        'rate' => '0.98'
                    ]
                ]
            ]
        ];

        // Set up the expected method calls on the mock objects
        $httpClientMock->expects($this->once())
            ->method('request')
            ->with($this->equalTo('GET'), $this->equalTo('https://api.nbp.pl/api/exchangerates/tables/A'))
            ->willReturn($responseMock);

        $responseMock->expects($this->once())
            ->method('toArray')
            ->willReturn($responseData);

        // Create an instance of the NBPApiClient and call the method we want to test
        $nbpApiClient = new NBPApiClient($httpClientMock, 'https://api.nbp.pl/api/exchangerates/tables/A');
        $result = $nbpApiClient->fetch();

        // Assert that the result is the expected response data
        $this->assertEquals($responseData, $result);
    }

    public function testFetchWithException()
    {
        // Mock the HttpClientInterface dependency and throw an exception
        $httpClientMock = $this->getMockBuilder(HttpClientInterface::class)
            ->getMock();

        $httpClientMock->expects($this->once())
            ->method('request')
            ->willThrowException(new Exception('Failed to fetch data'));

        // Create an instance of the NBPApiClient and call the method we want to test
        $nbpApiClient = new NBPApiClient($httpClientMock, 'https://api.nbp.pl/api/exchangerates/tables/A');
        $result = $nbpApiClient->fetch();

        // Assert that the result is an empty array
        $this->assertIsArray($result);
        $this->assertEmpty($result);
    }
}
