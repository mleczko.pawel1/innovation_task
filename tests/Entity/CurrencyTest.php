<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Currency;
use App\Entity\ValueObject\ExchangeRate;
use Money\Currency as MoneyCurrency;
use Money\Money;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class CurrencyTest extends TestCase
{
    public function testCreateCurrency(): void
    {
        $currencyName = 'US Dollar';
        $currency = new Currency($currencyName);

        $this->assertSame($currencyName, $currency->getName());
        $this->assertTrue(Uuid::isValid($currency->getId()));
    }

    public function testSetExchangeRate(): void
    {
        $currency = new Currency('US Dollar');
        $exchangeRate = new ExchangeRate(new Money(200, new MoneyCurrency('USD')));
        $currency->setExchangeRate($exchangeRate);

        $this->assertSame($exchangeRate, $currency->getExchangeRate());
    }
}

