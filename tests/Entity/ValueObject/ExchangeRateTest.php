<?php

declare(strict_types=1);

namespace App\Tests\Entity\ValueObject;

use App\Entity\ValueObject\ExchangeRate;
use Money\Currency;
use Money\Money;
use PHPUnit\Framework\TestCase;

class ExchangeRateTest extends TestCase
{
    public function testExchangeRate(): void
    {
        $moneyExchangeRate = new Money(200, new Currency('USD'));
        $exchangeRate = new ExchangeRate($moneyExchangeRate);

        $this->assertSame('200', $exchangeRate->getExchangeRate());
        $this->assertSame('USD', $exchangeRate->getCurrencyCode()->getCode());
    }
}
