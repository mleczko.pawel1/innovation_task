<?php

declare(strict_types=1);

namespace App\Tests\Command;

use App\Command\FetchExchangeRatesCommand;
use App\Service\ExchangeRatesServiceInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class FetchExchangeRatesCommandTest extends KernelTestCase
{
    private CommandTester $commandTester;

    public function testExecute(): void
    {
        $this->commandTester->execute([]);

        $this->commandTester->assertCommandIsSuccessful();
    }

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $exchangeRateService = $this->createMock(ExchangeRatesServiceInterface::class);

        $application->add(new FetchExchangeRatesCommand($exchangeRateService));

        $command = $application->find('app:exchange-rates:fetch');
        $this->commandTester = new CommandTester($command);

        parent::setUp();
    }
}

