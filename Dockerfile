FROM php:8.2-fpm
RUN apt update && \
    apt install -y git unzip zlib1g-dev libicu-dev g++ --no-install-recommends && \
    docker-php-ext-configure intl && \
    docker-php-ext-install bcmath pdo pdo_mysql intl && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
COPY . /app
WORKDIR /app

RUN composer install