<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Currency;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CurrencyRepository extends ServiceEntityRepository implements CurrencyRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Currency::class);
    }

    public function save(Currency $currency): void
    {
        $this->getEntityManager()->persist($currency);
        $this->getEntityManager()->flush();
    }

    public function getByCurrencyCode(string $currencyCode): ?Currency
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder->select('c')
            ->from($this->getClassName(), 'c')
            ->where($queryBuilder->expr()->eq('c.exchangeRate.currencyCode', ':currencyCode'))
            ->setParameter(':currencyCode', $currencyCode);

        return $queryBuilder->getQuery()
            ->getOneOrNullResult();
    }
}
