<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Currency;

interface CurrencyRepositoryInterface
{
    public function save(Currency $currency): void;

    public function getByCurrencyCode(string $currencyCode): ?Currency;
}
