<?php

declare(strict_types=1);

namespace App\Service;

interface ExchangeRatesClientInterface
{
    public function fetch(): array;
}
