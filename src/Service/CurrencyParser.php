<?php

declare(strict_types=1);

namespace App\Service;

use Money\Currencies\ISOCurrencies;
use Money\Currency as MoneyCurrency;
use Money\Money;
use Money\Parser\IntlLocalizedDecimalParser;
use NumberFormatter;

class CurrencyParser implements CurrencyParserInterface
{
    public function parse(array $rawCurrencyData): Money
    {
        $currencies = new ISOCurrencies();

        $numberFormatter = new NumberFormatter('pl_PL', NumberFormatter::DECIMAL);
        $moneyParser = new IntlLocalizedDecimalParser($numberFormatter, $currencies);

        return $moneyParser->parse($this->prepareAmount($rawCurrencyData['mid']), new MoneyCurrency($rawCurrencyData['code']));
    }

    private function prepareAmount(float $amount): string
    {
        return str_replace('.', ',', (string) $amount);
    }
}
