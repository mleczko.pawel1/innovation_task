<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Currency;
use App\Entity\ValueObject\ExchangeRate;
use App\Repository\CurrencyRepositoryInterface;

class ExchangeRatesService implements ExchangeRatesServiceInterface
{
    public function __construct(
        private readonly ExchangeRatesClientInterface $client,
        private readonly CurrencyRepositoryInterface $currencyRepository,
        private readonly CurrencyParserInterface $currencyParser
    ) {
    }

    public function processRates(): void
    {
        $exchangeRatesRawData = $this->client->fetch();
        $exchangeRatesArray = $exchangeRatesRawData[0]['rates'];

        foreach ($exchangeRatesArray as $rawExchangeRate) {
            $currency = $this->currencyRepository->getByCurrencyCode($rawExchangeRate['code']);

            if (! $currency) {
                $currency = new Currency($rawExchangeRate['currency']);
            }

            $exchangeRate = new ExchangeRate($this->currencyParser->parse($rawExchangeRate));

            $currency->setExchangeRate($exchangeRate);

            $this->currencyRepository->save($currency);
        }
    }
}
