<?php

declare(strict_types=1);

namespace App\Service;

use Money\Money;

interface CurrencyParserInterface
{
    public function parse(array $rawCurrencyData): Money;
}
