<?php

declare(strict_types=1);

namespace App\Service;

use Exception;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class NBPApiClient implements ExchangeRatesClientInterface
{
    public function __construct(
        private readonly HttpClientInterface $client,
        private readonly string $apiUri
    ) {
    }

    public function fetch(): array
    {
        try {
            $response = $this->client->request('GET', $this->apiUri);

            return $response->toArray();
        } catch (Exception) {
            return [];
        }
    }
}
