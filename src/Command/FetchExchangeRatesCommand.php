<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\ExchangeRatesServiceInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:exchange-rates:fetch',
    description: 'Fetch exchange rates and save to DB'
)]
class FetchExchangeRatesCommand extends Command
{
    public function __construct(
        private readonly ExchangeRatesServiceInterface $exchangeRatesService
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->exchangeRatesService->processRates();

        return Command::SUCCESS;
    }
}
