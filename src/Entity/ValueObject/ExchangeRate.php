<?php

declare(strict_types=1);

namespace App\Entity\ValueObject;

use Doctrine\ORM\Mapping as ORM;
use Money\Currency;
use Money\Money;

#[ORM\Embeddable]
final class ExchangeRate
{
    #[ORM\Column(nullable: false)]
    private string $exchangeRate;

    #[ORM\Column(type: "currency")]
    private Currency $currencyCode;

    public function __construct(Money $moneyExchangeRate)
    {
        $this->exchangeRate = $moneyExchangeRate->getAmount();
        $this->currencyCode = $moneyExchangeRate->getCurrency();
    }

    public function getExchangeRate(): string
    {
        return $this->exchangeRate;
    }


    public function getCurrencyCode(): Currency
    {
        return $this->currencyCode;
    }
}
