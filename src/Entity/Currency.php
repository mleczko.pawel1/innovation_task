<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\ValueObject\ExchangeRate;
use App\Repository\CurrencyRepositoryInterface;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

#[ORM\Entity(repositoryClass: CurrencyRepositoryInterface::class)]
final class Currency
{
    #[ORM\Id]
    #[ORM\Column(type: "guid", unique: true)]
    private string $id;

    #[ORM\Embedded(class: ExchangeRate::class, columnPrefix: false)]
    private ExchangeRate $exchangeRate;

    /**
     * @param string $name
     */
    public function __construct(
        #[ORM\Column(length: 50)]
        private readonly string $name
    ) {
        $this->id = (string) Uuid::uuid4();
    }

    public function setExchangeRate(ExchangeRate $exchangeRate): void
    {
        $this->exchangeRate = $exchangeRate;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getExchangeRate(): ExchangeRate
    {
        return $this->exchangeRate;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
